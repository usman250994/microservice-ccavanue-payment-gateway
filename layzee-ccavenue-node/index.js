require('dotenv').config()
const express = require('express');
var bodyParser = require('body-parser')
const morgan = require('morgan')
const app = express();
app.use(bodyParser.json());
app.use(morgan('combined'));
const path = require('path');
const router = express.Router();
const axios = require('axios');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use('/', router);

router.get('/', function (req, res) {
    const { jobId, userToken } = req.query;
    if (!jobId || !userToken) {
        res.status(400).send('bad request');
    }
    try {
        axios.get(process.env.STAGING_SERVER + "/job/getJobById?jid=" + jobId, {
            headers: { Authorization: "Bearer " + userToken }
        }).then((resp) => {
            axios.post(process.env.STAGING_SERVER + "/job/generateJobPaymentToken",
                { _id: jobId },
                {
                    headers: {
                        Authorization: "Bearer " + userToken,
                        'Content-Type': 'application/json'
                    },
                }
            ).then((response) => {
                res.render('ccavanue-user-end.html',
                    {
                        merchantID: process.env.MERCHANT_ID,
                        amountPayable: resp.data.data.taxationDetails.customerCollection,
                        orderID: `${jobId}_${response.data.data.paymentToken}`,
                        phpServer: process.env.PHP_SERVER

                    });
            });
        })
    } catch (e) {
        console.log("message", e)
    }
});

app.listen(process.env.PORT);

console.log('Running at Port', process.env.PORT);
